REST API template using flask_restplus, sqlalchemy and swagger

This is meant to be a means to a fast implementation of a database-backed REST API
with full automation; meaning, you point it at a database and it more-or-less automatically
produces a full REST API for that db (subject to allowing tweaks as necessary/desired).

# To start server:
flask_here TESTING
flask run

# - TODO:
- copy manage.py, alembic, pbutils from ~/Dropbox/sandbox/python/docker-postgres-flask; plus
  others supporting files/folders.
- connect to different db types (eg postgres) (why???)
- flask_login/auth0
- easy namespace additions
- docker-compose deployment w/postgres and 12factor.app:
- - Create Dockerfile
- cloud deployment
see notes.org

# references:
Dockerizing flask apps:
https://medium.com/bitcraft/docker-composing-a-python-3-flask-app-line-by-line-93b721105777

Building Dockers using private git repos:
https://vsupalov.com/build-docker-image-clone-private-repo-ssh-key/

Building multistage Docker images and virtualenv to reduce image size:
https://pythonspeed.com/articles/multi-stage-docker-python/
