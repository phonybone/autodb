"""
polyglot_table

Revision ID: 10d9dbb5b198
Revises: b16e4d42def9
Create Date: 2019-04-19 20:25:56.156096

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '10d9dbb5b198'
down_revision = 'b16e4d42def9'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
