"""002_create_order_table

Revision ID: b16e4d42def9
Revises: 8f85d285604e
Create Date: 2018-12-15 10:20:57.443974

"""
from alembic import op
import sqlalchemy as sa
import datetime as dt

# revision identifiers, used by Alembic.
revision = 'b16e4d42def9'
down_revision = '8f85d285604e'
branch_labels = None
depends_on = None


def upgrade():
    order_table = op.create_table(
        'orders',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('timestamp', sa.DateTime, nullable=False, default=dt.datetime.now()),
    )


def downgrade():
    op.drop_table('orders')
