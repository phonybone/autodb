import logging

from flask import request
from flask_restplus import Resource

from booklist.api.restplus import api
# from booklist.database.models import User, Book
# from booklist.api.bklist.business import get_user, delete_user, get_book, create_book, get_user_book, add_to_booklist, delete_from_booklist
# from booklist.api.bklist.serializers import user, book, page_of_users, page_of_books
# from booklist.api.bklist.parsers import pagination_arguments
# from booklist.utils import get_form_data, ppjson

ns = api.namespace('api', description='Operations related a single user')


@ns.route('/user/<int:id>')
@api.response(404, 'User not found.')
class UserItem(Resource):

    @api.marshal_with(user)
    def get(self, id):
        ''' Return a user. '''
        return get_user(id)

    @api.expect(user)
    @api.response(204, 'User successfully updated.')
    def put(self, id):
        ''' Update a user. '''
        data = request.json
        update_user(id, data)
        return None, 204

    @api.response(204, 'User successfully deleted.')
    def delete(self, id):
        ''' Deletes a user. '''
        delete_user(id)
        return None, 204

########################################################################

@ns.route('/user/<int:id>/books')
@api.response(404, 'User not found.')
class UserBooks(Resource):

    @api.expect(pagination_arguments) 
    @api.marshal_with(page_of_books)
    def get(self, id):
        ''' get books on user's booklist '''
        user = get_user(id)
        args = pagination_arguments.parse_args(request)
        page = args.get('page', 1)
        per_page = args.get('per_page', 10)
        return Book.query.filter(Book.users.any(id=user.id)).paginate(page, per_page, error_out=False)

    @api.response(201, 'Book successfully created and added to list.')
    @api.response(204, 'Book successfully added to list.')
    def post(self, id):
        ''' add a book to user's booklist (create book if book info provided) '''
        book_data = get_form_data(request)
        status_code = add_to_booklist(id, book_data)
        return None, status_code
    
@ns.route('/user/<int:user_id>/book/<int:book_id>')
@api.response(404, 'User or book not found.')
class UserBook(Resource):
    
    @api.marshal_with(book)
    def get(self, user_id, book_id):
        ''' get a specific book from user's list '''
        return get_user_book(user_id, book_id)

    @api.response(204, 'Book successfully removed from list.')
    def delete(self, user_id, book_id):
        ''' remove a book from user's booklist '''
        delete_from_booklist(user_id, book_id)
        return None, 204
    

