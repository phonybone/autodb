'''
Define/initialize high-level api object.
Also define some error handlers.
'''

from flask_restplus import Api
from sqlalchemy.orm.exc import NoResultFound
from autodb.logger import log

api = Api(version='1.0', title='Autodb', description='Autodb')


@api.errorhandler
def default_error_handler(e):
    message = 'An unhandled exception occurred.'
    log.exception(message)
    return {'message': message}, 500


@api.errorhandler(NoResultFound)
def database_not_found_error_handler(e):
    log.exception(e)
    return {'message': 'A database result was required but none was found.'}, 404
