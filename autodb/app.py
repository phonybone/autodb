'''
Define/initialize app.
Create & register blueprint, add namespaces to blueprint.
call db.init_app()
'''

import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restplus import Api

from autodb.database.table2ns import make_namespace
from autodb.middleware import Middleware
from autodb.database import import_tables, get_conn, close_conn
from autodb.logger import log


# Let's get started:
app = Flask(__name__)
app.wsgi_app = Middleware(app.wsgi_app)
api = Api(app, version='1.0', title='Autodb', description='Autodb')


# Config
env_src = 'autodb.config.' + os.environ.get('FLASK_ENV', 'Development').capitalize()
app.config.from_object(env_src)
app.debug = app.config['DEBUG']
app.testing = app.config['TESTING']
# app.wsgi_app = Middleware(app.wsgi_app)


def initialize_autodb(app, conn):
    # Database:
    db_url = app.config['SQLALCHEMY_DATABASE_URI']
    log.info(F"Using database {db_url}")
    if 'sqlite:///' in db_url:
        db_file = db_url.replace('sqlite:///', '')
        if not os.path.exists(db_file):
            raise OSError(F"{db_file} doesn't exist")
    app.sqla = SQLAlchemy(app)

    # Namespaces (one per table)
    tables = import_tables(app.sqla)
    log.info(F"got {len(tables)} tables")
    for tname, table in tables.items():
        if 'alembic' in tname:
            continue
        make_namespace(api, table, conn)

    dump_rules(app)
    app.teardown_appcontext(close_conn)


def dump_rules(app):
    def has_no_empty_params(rule):
        defaults = rule.defaults if rule.defaults is not None else ()
        arguments = rule.arguments if rule.arguments is not None else ()
        return len(defaults) >= len(arguments)

    for rule in app.url_map.iter_rules():
        # Filter out rules we can't navigate to in a browser
        # and rules that require parameters
        if "GET" in rule.methods and has_no_empty_params(rule):
            # url = url_for(rule.endpoint, **(rule.defaults or {}))
            url = rule.rule
            # links.append((url, rule.endpoint))
            log.info(F"route: {url}, -{rule.endpoint}-")


with app.app_context():
    conn = get_conn()
    initialize_autodb(app, conn)


if __name__ == '__main__':
    log.info('app.py being run directly from command line')
    app.run(debug=True)
