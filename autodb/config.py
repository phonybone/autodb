'''
Config info for autodb.
'''
import os
root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

# let's add a random comment

class Config:
    # SERVER_NAME = 'autodb.victorcassen.com'
    # SERVER_HOST = "0.0.0.0"
    # SERVER_PORT = 5000
    # SERVER_THREADS = 2
    DEBUG = True
    TESTING = False

    SQLALCHEMY_DATABASE_URI = f'sqlite:///{root}/autodb/auto.db'
    # SQLALCHEMY_DATABASE_URI = f'sqlite:///{os.environ["HOME"]}/Dropbox/sandbox/python/flasks/autodb/auto.db'
    # SQLALCHEMY_DATABASE_URI = 'postgres://localhost:5432/autodb'
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class Production(Config):
    """
    Production config
    """
    DEBUG = False


class Development(Config):
    """
    Development config
    """
    # SERVER_NAME = 'localhost'


class Testing(Config):
    """
    Testing config
    """
    # pass
    # SERVER_NAME = 'localhost'
    TESTING = True
    SQLALCHEMY_DATABASE_URI = F'sqlite:///{root}/autodb/test.db'
    # # SQLALCHEMY_DATABASE_URI = f'sqlite:///{os.environ["HOME"]}/Dropbox/sandbox/python/flasks/autodb/test.db'
