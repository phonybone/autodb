'''
Init db.
helper method to reset db.
'''
from flask import current_app, g  # see http://flask.pocoo.org/docs/1.0/tutorial/database/
from pbutils.sqla_core import init_sqla
from autodb.logger import log


def get_conn():
    if 'conn' not in g:
        g.engine, g.meta, g.conn = init_sqla(current_app.config['SQLALCHEMY_DATABASE_URI'])
    return g.conn


def close_conn(arg):
    if arg is not None:
        log.info(F"teardown: arg is {arg}")

    conn = g.pop('conn', None)
    if conn is not None:
        conn.close()


def import_tables(sqla):
    '''
    Reflect all tables in db.
    Return a dictionary: k=tablename, v=Table obj
    @param sqla: SQLAlchemy(app)
    '''
    meta = sqla.metadata
    engine = sqla.engine
    meta.reflect(bind=engine)
    return meta.tables


def dump_tables(tables):
    for tablename, table in tables.items():
        log.info(f"table {table.name}")
        if not is_top_level(table):
            continue

        for col in table.c.values():
            log.debug(f"{col.name} {col.type} nullable={col.nullable} pk={col.primary_key} unique={col.unique}")
            if col.foreign_keys:
                log.debug(f"fkeys: {col.foreign_keys}")


def is_top_level(table):
    for col in table.c.values():
        if col.foreign_keys:
            return False
    return True
