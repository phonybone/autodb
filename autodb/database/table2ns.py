'''
Create a resource namespace from a table.
'''

# Required steps:
# create an api.namespace object
# create an api.model object based on the table
# create a business logic object based on sqla_core.SimpleStore; this can be done at "run" time
#   (ie, when responding to the HTTP request).
# Create two resource classes, one for items and one for lists of items; add these to the
# namespace using ns.route().


from flask import abort, request, url_for
from flask_restplus import Resource, fields
import sqlalchemy as sa

from pbutils.sqla_core import SimpleStore
from autodb.database import get_conn
from autodb.utils import immd2dict
from autodb.logger import log


def make_namespace(api, table, conn):
    '''
    Create a namespace for the given table.

    The namespace supports two endpoints: '/' and '/<pk>' where pk is the name of the primary field key.
    The '/' endpoints supports GET and POST requests.
    The '/<pk>' suports GET, PUT, and DELETE.
    '''
    log.info(F"making namespace for {table.name}")
    ns = api.namespace(table.name, description=F"{table.name} operations")
    model = make_model(api, table)
    dao_cls = make_dao_cls(table)

    list_class = make_list_resource(api, ns, table, model, dao_cls)
    list_class = ns.route('/', endpoint=f'{table.name}s')(list_class)

    dao = dao_cls(conn, table)
    pk = dao.primary_keys[0].name
    item_class = make_item_resource(api, ns, table, model, dao_cls)

    # apply decorators manually:
    item_class = ns.route(f'/<{pk}>', endpoint=table.name)(item_class)
    item_class = ns.response(404, f'{table.name} not found')(item_class)
    item_class = ns.param(pk, f'The {table.name} {pk}')(item_class)
    return ns


# map sqlalchemy types to flask_restplus fields:
col2field = {
    sa.Boolean: fields.Boolean,
    sa.BOOLEAN: fields.Boolean,

    sa.Date: fields.Date,
    sa.DATE: fields.Date,
    sa.DateTime: fields.DateTime,
    sa.DATETIME: fields.DateTime,

    sa.Float: fields.Float,
    sa.FLOAT: fields.Float,
    sa.Integer: fields.Integer,
    sa.INTEGER: fields.Integer,

    sa.String: fields.String,
    sa.Text: fields.String,
    sa.TEXT: fields.String,
    sa.VARCHAR: fields.String,
    # sa.ForeignKey: None,
    # sa.Time: None,
    # sa.JSON: None,
    # sa.Numeric: None,
}


def make_model(api, table):
    '''
    Translate a Table object into an api.model()
    '''
    args = {}
    errors = []
    for colname, col in table.c.items():
        if type(col.type) in col2field:
            args[colname] = col2field[type(col.type)](description=colname)
        else:
            errors.append(F"Don't know how to model {table.name}.{colname}: type={col.type}")
    if errors:
        raise RuntimeError("\n".join(errors))
    return api.model(table.name.capitalize(), args)


def make_dao_cls(table):
    '''
    Create a DAO object for a table.
    @param table: a sqlalchemy Table object
    @param conn: a sqlalachemy Connection (as returned by engine.connect()
    '''
    methods = {
        '__init__': SimpleStore.__init__,
    }
    dao_cls = type(F"{table.name.capitalize()}DAO", (SimpleStore,), methods)
    return dao_cls


def make_list_resource(api, ns, table, model, dao_cls):
    def make_get_list(api, ns, model):
        def get(self, **kwargs):
            conn = get_conn()
            dao = dao_cls(conn, table)
            # covert request.args (ImmutableMultiDict) into regular dict; hacky:
            where = immd2dict(request.args)
            results = dao.get(**where)
            return list(results), 200
        get.__doc__ = F"List all {table.name}"
        get = ns.doc(f'list_{table.name}')(get)
        get = ns.marshal_list_with(model)(get)
        return get

    def make_post_item(api, ns, model):
        def post(self, **kwargs):
            conn = get_conn()
            dao = dao_cls(conn, table)

            obj_id = dao.insert(request.json)
            url_for_kwargs = {dao.primary_keys[0].name: obj_id}  # pk_col name -> pk
            resp_data = {'url': url_for(endpoint=f'{table.name}', **url_for_kwargs)}
            return resp_data, 201
        return post

    ListResource = type('f{table.name}ListResource', (Resource,), {
        'get': make_get_list(api, ns, model),
        'post': make_post_item(api, ns, model),
    })

    return ListResource


def make_item_resource(api, ns, table, model, dao_cls):
    def make_get_item(api, ns, model):
        def get(self, **kwargs):
            conn = get_conn()
            dao = dao_cls(conn, table)

            pk_colname = dao.primary_keys[0].name
            pk = kwargs.pop(pk_colname)
            obj = dao.get_pk(pk)
            if obj is None:
                abort(404)
            return obj
        get.__doc__ = F"get {table.name}"
        get = ns.doc(f'get_{table.name}')(get)
        get = ns.marshal_with(model)(get)
        return get

    def make_put_item(api, ns, model):
        def put(self, **kwargs):
            conn = get_conn()
            dao = dao_cls(conn, table)

            pk_colname = dao.primary_keys[0].name
            pk = kwargs.pop(pk_colname)
            obj = dao.get_pk(pk)
            if obj is None:
                abort(404)

            obj = dict(obj.items())
            obj.pop(pk_colname)
            obj.update(request.json)
            dao.update(pk, obj)
            return obj, 202
        put.__doc__ = F"put {table.name}"
        put = ns.expect(model)(put)
        put = ns.marshal_with(model)(put)
        return put

    def make_delete_item(ns, model):
        def delete(self, id):
            conn = get_conn()
            dao = dao_cls(conn, table)
            dao.get_pk(id)  # just to abort if not found
            dao.delete(id)
            return '', 204

    ItemResource = type(f'{table.name}ItemResource', (Resource,), {
        'get': make_get_item(api, ns, model),
        'put': make_put_item(api, ns, model),
        # todo: DELETE

    })

    return ItemResource
