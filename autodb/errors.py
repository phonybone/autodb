'''
Error classes and error handlers.
'''


class NotFoundError(ValueError):
    pass
