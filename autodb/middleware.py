'''
Playing with middleware.
'''
# see https://ohadp.com/adding-a-simple-middleware-to-your-flask-application-in-1-minutes-89782de379a1

# from flask import Request
# from autodb.logger import log


class Middleware:
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        # do something
        # req = Request(environ)
        # log.debug(F"req: {req}")
        return self.app(environ, start_response)
