
# # Todos ################################################################


# ns = api.namespace('Todos', description='TODO operations')

# todo_model = api.model('Todo', {
#     'id': fields.Integer(readOnly=True, description='The task unique identifier'),
#     'task': fields.String(required=True, description='The task details')
# })


# class TodoDAO(object):
#     def __init__(self):
#         self.counter = 0
#         self.todos = []

#     def get(self, id):
#         for todo in self.todos:
#             if todo['id'] == id:
#                 return todo
#         api.abort(404, "Todo {} doesn't exist".format(id))

#     def create(self, data):
#         todo = data
#         todo['id'] = self.counter = self.counter + 1
#         self.todos.append(todo)
#         return todo

#     def update(self, id, data):
#         todo = self.get(id)
#         todo.update(data)
#         return todo

#     def delete(self, id):
#         todo = self.get(id)
#         self.todos.remove(todo)


# todoDAO = TodoDAO()
# todoDAO.create({'task': 'Build an API'})
# todoDAO.create({'task': '?????'})
# todoDAO.create({'task': 'profit!'})


# @ns.route('/')
# class TodoList(Resource):
#     '''Shows a list of all todos, and lets you POST to add new tasks'''
#     @ns.doc('list_todos')
#     @ns.marshal_list_with(todo_model)
#     def get(self):
#         '''List all tasks'''
#         return todoDAO.todos

#     @ns.doc('create_todo')
#     @ns.expect(todo_model)
#     @ns.marshal_with(todo_model, code=201)
#     def post(self):
#         '''Create a new task'''
#         return todoDAO.create(api.payload), 201


# # @ns.route('/<int:id>')
# # @ns.response(404, 'Todo not found, ok?')
# # @ns.param('id', 'The task id')
# class Todo(Resource):
#     '''Show a single todo item and lets you delete them'''
#     @ns.doc('get_todo')
#     @ns.marshal_with(todo_model)
#     def get(self, id):
#         '''Fetch a given resource'''
#         return todoDAO.get(id)

#     @ns.doc('delete_todo')
#     @ns.response(204, 'Todo deleted')
#     def delete(self, id):
#         '''Delete a task given its identifier'''
#         todoDAO.delete(id)
#         return '', 204

#     @ns.expect(todo_model)
#     @ns.marshal_with(todo_model)
#     def put(self, id):
#         '''Update a task given its identifier'''
#         return todoDAO.update(id, api.payload)


# ns.route('/<int:id>')(Todo)
# ns.response(404, 'Todo not found, ok?')(Todo)
# ns.param('id', 'The task id')(Todo)

########################################################################



# from flask_restplus import Resource, fields
ns = api.namespace('things', decription='things desc')
thing_model = api.model('Things', {
    'id': fields.Integer(required=True),
    'value': fields.String(required=True),
})
all_things = [
    {'id': 1, 'value': 'v1'},
    {'id': 2, 'value': 'v2'},
]
class ThingList(Resource):
    @ns.doc('get_all_the_things')
    @ns.marshal_list_with(thing_model)
    def get():
        return all_things

ns.route('/')(ThingList)


# from user import make_user_ns, make_user_model, make_user_list, make_user_item
# user_ns = make_user_ns(api)
# user_model = make_user_model(api)

# UserList = make_user_list(user_ns, user_model, api)
# user_ns.route('/')(UserList)

# UserItem = make_user_item(user_ns, user_model, api)
# UserItem = user_ns.route('/<int:id>')(UserItem)
# UserItem = user_ns.response(404, 'User not found')(UserItem)
# UserItem = user_ns.param('id', 'The user id')(UserItem)
