'''
Miscelaneous utilities
'''


def immd2dict(immd):
    '''
    Convert an ImmutableMultiDict to a regular dict by taking the first item of any list as value.
    This obviously throws away information but is useful when you know that none of the inputs
    is multi-values (eg for simple search functionality).
    '''
    dct = {}
    for k, v in immd.items():
        if isinstance(v, list):
            dct[k] = v[0]
        else:
            dct[k] = v
    return dct
