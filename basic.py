"""
This is a sandbox script to test ideas.
"""
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restplus import fields
# from sqlalchemy import Table

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////Users/victor.cassen/Dropbox/sandbox/python/flasks/autodb/autodb/test.db'
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///autodb/test.db'
# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://localhost:5432/autodb'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

meta = db.metadata
engine = db.engine
meta.reflect(bind=engine)

# account = Table('account', meta, autoload=True, autoload_with=engine)
# print(f'got table {account}')


models = {}



for tablename, table in meta.tables.items():
    print(f'got tablename={tablename}, table={table}')
    model_args = {}
    for colname, col in table.c.items():
        print(F"{colname}: type={col.type}")
        import pdb; pdb.set_trace()
        if hasattr(fields, col.type):
            print(F"direct mapping column.{col.type} -> fields.{col.type}")
        else:
            print(F"no direct mapping for {col.type}")
    print()
