from contextlib import contextmanager

from sqlalchemy import engine, Table, text
from sqlalchemy.exc import DatabaseError
import json

from pbutils.streams import records
from pbutils.sqla_core import do_stream

@contextmanager
def reset_table_data(db, table, data):
    """
    Set the contents of a Table to the given data, then yield, then cleanup the table.
    """
    db.execute(table.delete())
    if data:
        try:
            db.execute(table.insert().values(data))
        except DatabaseError as e:
            try:
                dump = json.dumps(data)
            except (ValueError, TypeError) as e:
                dump = str(data)
            raise
    yield
    db.execute(table.delete())


@contextmanager
def reset_table_sql(db, table, sql_fn):
    with open(sql_fn) as sql:
        do_stream(db, sql)
    yield
