'''
Tests for /account
'''
from pbutils.strings import qw


def test_get_accounts(client):
    resp = client.get('/account', follow_redirects=True)
    accounts = resp.json
    assert isinstance(accounts, list)
    assert len(accounts) > 0
    for account in accounts:
        assert is_account(account)


def test_get_account_1(client):
    resp = client.get('/account/wilma', follow_redirects=True)
    account1 = resp.json
    assert isinstance(account1, dict)
    assert is_account(account1)


def is_account(obj):
    for key in qw('username emai passwordl fname lname'):
        return key in obj
    return True
