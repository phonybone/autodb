import json


def test_post_object(client, conn, account3_sql):
    '''
    Inserts an account into the db, then fetches from db it and compares against original data.
    '''
    new_accnt = {
        'username': 'gjetson',
        'email': 'george.jetson@future.com',
        'fname': 'George',
        'lname': 'Jetson',
        'password': 'elroy',
    }
    resp = client.post('/account/', data=json.dumps(new_accnt), content_type='application/json')
    assert resp.status_code == 201

    engine, meta, connection = conn
    george = connection.execute('SELECT * FROM account WHERE username="gjetson"').first()
    for k, v in new_accnt.items():
        assert george[k] == new_accnt[k]
