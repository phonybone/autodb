import json


def test_put_account(client, conn, account3_sql):
    resp = client.put('/account/wilma', data=json.dumps({'email': 'newwilma@mailinator.com'}), content_type='application/json')
    assert resp.status_code == 202

    engine, meta, connection = conn
    wilma = connection.execute('SELECT * FROM account WHERE username="wilma"').first()
    assert wilma['email'] == 'newwilma@mailinator.com'
