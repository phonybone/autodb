def test_search_accounts(client, account3_sql):
    resp = client.get('/account?username=wilma', follow_redirects=True)
    assert resp.status_code == 200
    accounts = resp.json
    assert isinstance(accounts, list)
    assert len(accounts) == 1
    account = accounts[0]
    assert account['username'] == 'wilma'
