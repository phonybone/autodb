import pytest
import pkg_resources as pr
from flask_restplus import Api

from pbutils.sqla_core import init_sqla
from autodb.app import app
from autodb.database import import_tables

from tests import reset_table_sql


@pytest.fixture
def client():
    yield app.test_client()


@pytest.fixture(scope='session')
def conn():
    ''' init_sqla returns engine, meta, conn '''
    db_url = app.config['SQLALCHEMY_DATABASE_URI']
    yield init_sqla(db_url)


@pytest.fixture(scope='session')
def api():
    api = Api(version='1.0', title='Testing', description='Testing')
    yield api


@pytest.fixture(scope='session')
def tables(conn):
    db, meta, connection = conn
    tables = import_tables(app.sqla)
    yield tables


@pytest.fixture(scope='function')
def account3_sql(conn, tables):
    engine, meta, connection = conn
    sql_fn = pr.resource_filename(__name__, 'fixtures/account3.sql')
    with reset_table_sql(connection, tables['account'], sql_fn):
        yield
