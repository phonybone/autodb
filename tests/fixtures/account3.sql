PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
DROP TABLE IF EXISTS account;
CREATE TABLE account (
	username VARCHAR NOT NULL, 
	email VARCHAR NOT NULL, 
	password VARCHAR NOT NULL, 
	fname VARCHAR NOT NULL, 
	lname VARCHAR NOT NULL, 
	PRIMARY KEY (username), 
	UNIQUE (email)
);
INSERT INTO account VALUES('phonybone','vmc.swdev@gmail.com','3f2f4295a5eb6ad967b832d35e048852','Victor','Cassen');
INSERT INTO account VALUES('fred','fred@mailinator.com','42b02be38a9061f5a51df94a43a6f6dc','Fred','Flintstone');
INSERT INTO account VALUES('wilma','wilma@mailinator.com','37f8481a0c4b88cdb9d7f3c0cd68d278','Wilma','Flintstone');
COMMIT;
