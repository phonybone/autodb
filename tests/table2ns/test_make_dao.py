from autodb.app import app
from autodb.database import import_tables
from autodb.database.table2ns import make_dao_cls
from pbutils.sqla_core import SimpleStore


def test_make_dao_cls(conn):
    # get all tables, create a dao object for each one.
    tables = import_tables(app.sqla)
    assert len(tables) > 0

    for tablename, table in tables.items():
        dao_cls = make_dao_cls(table)
        assert issubclass(dao_cls, SimpleStore)
        # objs = list(dao.get())
        # if len(objs) > 0:
        #     assert
