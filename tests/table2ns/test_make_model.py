from autodb.app import app
from flask_restplus.model import Model
from autodb.database import import_tables
from autodb.database.table2ns import make_model


# Need a table that has every single type of column in it, for testing.

def test_make_model(api):
    # get all tables, create a model for each one.
    tables = import_tables(app.sqla)
    assert len(tables) > 0

    for tablename, table in tables.items():
        model = make_model(api, table)
        assert isinstance(model, Model)
