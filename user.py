from flask_restplus import Resource, fields
from logging import getLogger

from user_store import userDAO


log = getLogger(__name__)


def make_user_ns(api):
    return api.namespace('Users', description='User operations')


def make_user_model(api):
    user_model = api.model('User', {
        'id': fields.Integer(readOnly=True, description='The user unique identifier'),
        'fname': fields.String(required=True, description='first name'),
        'lname': fields.String(required=True, description='last name'),
        'dob': fields.Date(required=True, description='date of birth'),
    })
    return user_model


def make_user_list(ns, user_model, api):
    def make_get_list(ns, user_model, api):
        def get(self):
            return userDAO.users
        get.__doc__ = "List all users"
        get = ns.doc('list_users')(get)
        get = ns.marshal_list_with(user_model)(get)
        return get

    UserList = type('UserList', (Resource,), {
        'get': make_get_list(ns, user_model, api)
        # todo: POST
    })

    return UserList


def make_user_item(ns, user_model, api):
    def make_get_item(ns, user_model, api):
        def get(self, id):
            return userDAO.get(id)
        get.__doc__ = "get user"
        get = ns.doc('get_user')(get)
        get = ns.marshal_with(user_model)(get)
        return get

    UserItem = type('UserItem', (Resource,), {
        'get': make_get_item(ns, user_model, api),
        # todo: PUT, DELETE

    })

    return UserItem
