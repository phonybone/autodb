import datetime as dt
from errors import NotFoundError


class UserDAO(object):
    def __init__(self):
        self.counter = 0
        self.users = []

    def get(self, id):
        for user in self.users:
            if user['id'] == id:
                return user
        raise NotFoundError

    def create(self, data):
        user = data
        user['id'] = self.counter = self.counter + 1
        self.users.append(user)
        return user

    def update(self, id, data):
        user = self.get(id)
        user.update(data)
        return user

    def delete(self, id):
        user = self.get(id)
        self.users.remove(user)

userDAO = UserDAO()
userDAO.create({'fname': 'Victor', 'lname': 'Cassen', 'dob': dt.date(1966, 4, 11)})
userDAO.create({'fname': 'Cheryl', 'lname': 'Cassen', 'dob': dt.date(1961, 6, 14)})
userDAO.create({'fname': 'Coco', 'lname': 'Cassen', 'dob': dt.date(2010, 5, 25)})
