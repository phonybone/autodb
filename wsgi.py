'''
Try to run wsgi from command line.
This is just used for local testing.
'''

import sys
import os
sys.path.append(os.path.dirname(__file__))

from autodb.app import app

if __name__ == '__main__':
    app.run()
